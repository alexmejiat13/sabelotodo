
select t2.preguntaID 
from contestando t1 
inner join preguntas_contestando t2 on t1.contestandoID = t2.contestandoID 
where t1.contestandoID = 1;


SELECT pregunta 
FROM preguntas 
WHERE preguntaID = 1

SELECT * FROM preguntas t1 INNER JOIN respuestas t2 on t1.preguntaID = t2.preguntaID WHERE t1.preguntaID = 1

SELECT usuario, puntaje_prom FROM contestando ORDER by puntaje_prom desc

-----------------------------------------------------------------------------

DROP TABLE IF EXISTS Respuestas;
DROP TABLE IF EXISTS Preguntas_Contestando;
DROP TABLE IF EXISTS Contestando;
DROP TABLE IF EXISTS Preguntas;

CREATE TABLE Preguntas (
    preguntaID int NOT NULL auto_increment,
    pregunta varchar(100),
    dificultad int,
    puntaje int,
    PRIMARY KEY (preguntaID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE Respuestas (
    respuestaID int NOT NULL auto_increment,
    preguntaID int NOT NULL,
    respuesta varchar(100),
    correcta boolean,
    PRIMARY KEY (respuestaID),
    FOREIGN KEY (preguntaID) REFERENCES Preguntas(preguntaID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE Contestando (
    contestandoID int NOT NULL auto_increment,
    usuario varchar(100),
    puntaje_prom double,
    PRIMARY KEY (contestandoID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE Preguntas_Contestando (
    preguntacontestadoID int NOT NULL auto_increment,
    preguntaID int NOT NULL,
    contestandoID int NOT NULL,
    correcta boolean,
    puntaje_acum int,
    contestada boolean,
    PRIMARY KEY (preguntacontestadoID),
    CONSTRAINT fk_pID FOREIGN KEY (preguntaID) REFERENCES Preguntas(preguntaID),
    CONSTRAINT fk_cID FOREIGN KEY (contestandoID) REFERENCES Contestando(contestandoID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (1, '¿Cuál son los pasos para que un algoritmo pueda ser implementado?', 7, 7);


INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
        (1, 1, 'Exacto,Complejo,Preciso', False),
        (2, 1, 'Preciso,Finito,Definido', False),
        (3, 1, 'Preciso,Definido,Finito', True);


INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (2, '¿Que procesos distigen un algoritmo?', 7, 7);


INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
        (4, 2, 'Entrada,Proceso,Salida', True),
        (5, 2, 'Entrada,Gestion,Finalización', False),
        (6, 2, 'Inicio,Gestion,Culminación', False);


INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (3, '¿Que es Pseudocodigo?', 7, 7);


INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
        (7, 3, 'Un lenguaje de programacióna', True),
        (8, 3, 'Un Diagrama de Flujo', False),
        (9, 3, 'Un Estandar para Representar Acciones', False);


INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (4, '¿para que se utiliza el condicional IF?', 7, 7);


INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
        (10, 4, 'Para Comprobar una condicion', True),
        (11, 4, 'Para Complementar un Ciclo', False),
        (12, 4, 'Para Validar si el algoritmo esta correcto', False);


INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (5, '¿Que es un Acumulador?', 7, 7);

INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
        (13, 5, 'Una variable que incrementa o decrementa su contenido en cantidades variables', True),
        (14, 5, 'Una variable que incrementa o decrementa su valor en un valor constante', False),
        (15, 5, 'Una Variable de Control que contabiliza las veces que entra al ciclo', False);

INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (6, '¿La Sentencia alternativa que puede acompañar al If es?', 7, 7);


INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
        (16, 6, 'If - while', False),
        (17, 6, 'If - Break', False),
        (18, 6, 'If - Else', True);


INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (7, '¿Que son condicionales anidados?', 8, 8);


INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
        (19, 7, 'Un bucle dentro del codigo', False),
        (20, 7, 'Una Sentencia por Fuera de Otra', False),
        (21, 7, 'Una Condición dentro de Otra Condición', True);


INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (8, '¿Cual condicional se usa para la toma de decisiones?', 7, 7);


INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
        (22, 8, 'while', False),
        (23, 8, 'If', True),
        (24, 8, 'For', False);


INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (9, '¿Que se Utilizaria para repetir un grupo de instrucciones?', 7, 7);


INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
        (25, 9, 'while', False),
        (26, 9, 'If', True),
        (27, 9, 'For', False);


INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (10, '¿Que es un ciclo While?', 7, 7);


INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
        (28, 10, 'Solo permite repetir un grupo de instrucciones', False),
        (29, 10, 'Permite repetir un grupo de instrucciones mientras la condicion es verdadera', True),
        (30, 10, 'Permite repetir un grupo de instrucciones mientras la condicion es Falsa', False);


INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (11, '¿Que es el ciclo DO-WHILE?', 7, 7);


INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
        (31, 11, 'Una Estructura de validacion en cadena que permite ejecutar 1 linea de codigo Iterativa', False),
        (32, 11, 'Una EsTructura de Control Ciclica que permite ejecutar varias lineas de codigo de forma Repetitiva', True),
        (33, 11, 'Una Estructura de repetición que permite ejecutar varios bloques de condiciones de forma Iterativa ', False);




INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (12, '¿Cual ciclo utilizamos cuando se conoce las veces que se a a repetir una condición?', 8, 8);


INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
        (34, 12, 'While', False),
        (35, 12, 'For', True),
        (36, 12, 'Do-While ', False);




INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (13, '¿Que es el ciclo For?', 7, 7);


INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
        (37, 13, 'Una Estructura de validacion en cadena que permite ejecutar 1 linea de codigo Iterativas', False),
        (38, 13, 'Una EsTructura de Control Ciclica que permite ejecutar varias lineas de codigo de forma Iterativa', True),
        (39, 13, 'Una Estructura de repetición que permite ejecutar varios bloques de condiciones de Forma Repetitiva ', False);


INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (14, '¿Que es un Contador?', 7, 7);


INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
        (40, 14, 'Un Ciclo que contabiliza un valor de manera repetitiva', False),
        (41, 14, 'Una variable que incrementa o decrementa su valor en un valor constante', True),
        (42, 14, 'una variable que incrementa o decrementa su contenido en cantidades variables', False);





