<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Preguntas extends CI_Controller
	{

		public function __construct()
		{
			parent::__construct();
			$this->load->model('preguntas_model');
			$this->load->helper('url');
			$this->load->library('calendar');

		    $this->load->library('xmlrpc');
		    $this->load->library('xmlrpcs');

		}

		public function api_results()
		{
	      	// set HTTP header
			$headers = array('Content-Type: application/json',);

			// the url of the API you are contacting to 'consume' 
			$url = 'http://[::1]/sabelotodo/api/api_sabelotodo/users'; 

			// Open connection
			$ch = curl_init();

			// Set the url, number of GET vars, GET data
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			// Execute request
			$result = curl_exec($ch);

			// Close connection
			curl_close($ch);

			// get the result and parse to JSON
			$items = json_decode($result);

			if(isset($items)){ return $items ; }

			else { return FALSE ; }


		}

		public function index()
		{

			$data['main_title'] = 'Sabelotodo';

			$this->load->helper('form');
			$this->load->library('form_validation');

			$this->form_validation->set_rules('usuario', 'Usuario', 'required');
			if ($this->form_validation->run() === FALSE)
			{
				$data['title2'] = 'Home';
				$this->load->view('templates/header', $data);
				$this->load->view('sabelotodo/home_view');
				$this->load->view('templates/footer');
			}
			else
			{
				
				
				$data['title2'] = 'Preguntas';
				$data['usuarioID'] = $this->input->post('usuarioID');
				$data['usuario'] = $this->input->post('usuario');
				$data['radio'] = $this->input->post('radio');

				if($data['radio'] == "")
				{
					$data['puntajeAcum'] = $this->input->post('puntaje');
					$data['usuarioID'] = $this->preguntas_model->add();	
				}
				else
				{	
					
					if($data['radio'] == 1)
					{
						$data['puntajeAcum'] = $this->input->post('acumulado') + $this->input->post('puntaje');
					}
					else
					{
						$data['puntajeAcum'] = $this->input->post('acumulado');
					}
					$this->preguntas_model->add_respuesta($data['puntajeAcum']);
				}

				$resultado = $this->preguntas_model->get_preguntascontestadas($data['usuarioID']);

				$this->load->view('templates/header_preguntas', $data);
				if(sizeof($resultado) < 14) 
				{
					if(count($resultado) > 0)
					{
						$idPregunta = $this->excluir($resultado);
					} 
					else
					{
						$idPregunta = mt_rand(1,14);
					}

					$data['datos'] = $this->preguntas_model->get_pregunta($idPregunta); 

					$this->load->view('sabelotodo/preguntas_view.php', $data);
				}
				else 
				{

					//$this->preguntas_model->get_clasificacion();
					$data['clasificacion'] = $this->api_results(); 

					$this->load->view('sabelotodo/resultado_view.php', $data);
				}
				$this->load->view('templates/footer_preguntas');

			}

		}

		public function excluir($arrExcluir)
		{	

			$aleatorio = mt_rand(1,14);
			$key = array_search($aleatorio, array_column($arrExcluir, 'preguntaID'));

			while (false !== $key){
				$aleatorio = mt_rand(1,14);
				$key = array_search($aleatorio, array_column($arrExcluir, 'preguntaID'));
			}

			return $aleatorio;

		}

	}
?>