<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	
		<!-- Contact -->
        <div class="agilecontactw3ls" id="w3lsaboutaits">
            <div class="container">
            	<div class="w3lsaboutaits-grids">
                    <div class="col-md-6 w3lsaboutaits-grid w3lsaboutaits-grid-1">
                <h3>¿Cómo jugar?</h3>
                <p>Ingresa un nombre y responde todas las preguntas de manera correcta. Reta a tus amigos y miren quien queda de primero en la clasificación.</p>
                <br>
                <p>Las preguntas son sobre lo que sabes de condicionales y ciclos.</p>
            </div>
        <div class="col-md-6 w3lsaboutaits-grid w3lsaboutaits-grid-2">
                        <img src="<?php echo base_url(); ?>images/questions.png" alt="Game Robo" height="350" width="200">
                    </div>
                    <div class="clearfix"></div></div>
            </div>
        </div>
        <!-- //Contact -->

        <!-- About -->
        <div class="w3lsaboutaits" id="agilecontactw3ls">
            <div class="container">
                <div class="w3lsaboutaits-grids">
                    <div class="col-md-6 w3lsaboutaits-grid w3lsaboutaits-grid-1">
                        <h3>Ciclos y condicionales</h3>
                        <p>Las condicionales en programacón son procedimientos para llevar a cabo una serie de instrucciones cuando se cumpla una sentencia o pregunta, estos van acompañados de operadores logicos.</p>
                        <p>Los ciclos, repeticiones o bucles se llevan a cabo cuando se desea que una serie de instrucciones se ejecuten varias veces cundo se cumpla la condición dada, ya sea por una opción de continuar o mediante una condición cumplida. Los ciclos mas usados en programacion son el for y el while.</p>
                    </div>
                    <div class="col-md-6 w3lsaboutaits-grid w3lsaboutaits-grid-2">
                        <img src="<?php echo base_url(); ?>images/con1.png" alt="Game Robo">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- //About -->

        

	<!-- Footer -->
	<div class="agilefooterwthree" id="agilefooterwthree">
		<div class="container">

			<div class="agilefooterwthreebottom">
				<div class="col-md-6 agilefooterwthreebottom-grid agilefooterwthreebottom-grid1">
					<div class="copyright">
						<p>© 2017 Game Robo. All Rights Reserved | Design by <a href="http://w3layouts.com/" target="=_blank"> W3layouts </a></p>
					</div>
				</div>
				<div class="col-md-6 agilefooterwthreebottom-grid agilefooterwthreebottom-grid2">
				</div>
			</div>

		</div>

		<a href="#agileitshome" class="agileto-top scroll" title="To Top"><img src="<?php echo base_url(); ?>images/to-top.png" alt="Game Robo"></a>

	</div>
	<!-- //Footer -->

		<!-- Custom-JavaScript-File-Links -->

		<!-- Default-JavaScript -->   
		<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-2.1.4.min.js"></script>
		<!-- Bootstrap-JavaScript --> 
		<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

		<!-- Resopnsive-Slider-JavaScript -->
			<script src="<?php echo base_url(); ?>js/responsiveslides.min.js"></script>
			<script>
				$(function () {
					$("#slider").responsiveSlides({
						auto: true,
						nav: true,
						speed: 2000,
						namespace: "callbacks",
						pager: true,
					});
				});
			</script>
		<!-- //Resopnsive-Slider-JavaScript -->

		<!-- Tab-JavaScript -->
			<script src="<?php echo base_url(); ?>js/cbpFWTabs.js"></script>
			<script>
				(function() {
					[].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
						new CBPFWTabs( el );
					});
				})();
			</script>
		<!-- //Tab-JavaScript -->

		<!-- Owl-Carousel-JavaScript -->
			<script src="<?php echo base_url(); ?>js/owl.carousel.js"></script>
			<script>
				$(document).ready(function() {
					$("#owl-demo, #owl-demo1, #owl-demo2, #owl-demo3, #owl-demo4").owlCarousel({
						autoPlay: 3000,
						items : 5,
						itemsDesktop : [1024,4],
						itemsDesktopSmall : [414,3]
					});
				});
			</script>
		<!-- //Owl-Carousel-JavaScript -->

		<!-- Stats-Number-Scroller-Animation-JavaScript -->
			<script src="<?php echo base_url(); ?>js/waypoints.min.js"></script> 
			<script src="<?php echo base_url(); ?>js/counterup.min.js"></script> 
			<script>
				jQuery(document).ready(function( $ ) {
					$('.counter').counterUp({
						delay: 10,
						time: 1000
					});
				});
			</script>
		<!-- //Stats-Number-Scroller-Animation-JavaScript -->

		<!-- Popup-Box-JavaScript -->
			<script src="<?php echo base_url(); ?>js/jquery.chocolat.js"></script>
			<script type="text/javascript">
				$(function() {
					$('.w3portfolioaits-item a').Chocolat();
				});
			</script>
		<!-- //Popup-Box-JavaScript -->

		<!-- Smooth-Scrolling-JavaScript -->
			<script type="text/javascript">
				jQuery(document).ready(function($) {
					$(".scroll").click(function(event){
						event.preventDefault();
						$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
					});
				});
			</script>
		<!-- //Smooth-Scrolling-JavaScript -->

	<!-- //Custom-JavaScript-File-Links -->

<Script language="javascript">
function deshabilitaRetroceso(){
    window.location.hash="no-back-button";
    window.location.hash="Again-No-back-button" //chrome
    window.onhashchange=function(){window.location.hash="no-back-button";}
}
</script>

</body>
<!-- //Body -->



</html>