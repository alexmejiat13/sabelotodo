<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Preguntas_Model extends CI_Model
{

	public function __construct()
    {
        $this->load->database();
    }

    public function add()
	{

		$data = array(
			'usuario'	=> $this->input->post('usuario'),
			'puntaje_prom'	=> 0,
		);

		$this->db->insert('contestando', $data);

		$ultimoId = $this->db->insert_id();

		return $ultimoId;		
	}

	public function add_respuesta($puntajeAcum)
	{
		$userID = $this->input->post('usuarioID');

		$data1 = array(
			'preguntaID' 	=> $this->input->post('preguntaID'),
			'contestandoID' 	=> $userID,
			'correcta'			=> $this->input->post('radio'),
			'contestada'		=> 1,
			'puntaje_acum'		=> $puntajeAcum,
		);

		$data2 = array(
			'puntaje_prom'		=> $puntajeAcum,
		);

		$this->db->insert('preguntas_contestando', $data1);

		$this->db->where('contestandoID', $userID);
		$this->db->update('contestando', $data2);
		
	}

	public function get_clasificacion()
	{
		//SELECT usuario, puntaje_prom FROM contestando ORDER by puntaje_prom desc
		$this->db->select('contestandoID, usuario, puntaje_prom');
		$this->db->from('contestando');
		$this->db->order_by('puntaje_prom', 'DESC');
		$this->db->limit(10);

		$result = $this->db->get();

		return $result->result_array();
	}

	public function get_preguntascontestadas($id)
	{

		$this->db->select('t2.preguntaID, t2.contestada');
		$this->db->from('contestando t1');
		$this->db->join('preguntas_contestando t2', 't1.contestandoID = t2.contestandoID');
		$this->db->where('t1.contestandoID', $id);

		$result = $this->db->get();

		return $result->result_array();
	}

	public function get_pregunta($id)
	{

		$this->db->select('t1.preguntaID, t1.pregunta, t1.dificultad, t1.puntaje, t2.respuestaID, t2.respuesta, t2.correcta');
		$this->db->from('preguntas t1');
		$this->db->join('respuestas t2', 't1.preguntaID = t2.preguntaID');
		$this->db->where('t1.preguntaID', $id);

		$result = $this->db->get();

		return $result->result_array();
	}

}
?>